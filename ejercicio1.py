# Ejercicio 1
print("\n¡Bienvenido al mundo de la programación!")
nom = input("\nPara empezar, ingresa tu nombre: ")

# Ejercicio 2
print(f"\n¡Bienvenido {nom}!")

# Ejercicio 3
print("\nIngrese una variable numérica X que resuelva la siguente ecuación (Debe ser un int)\n")
print(" (X**2) + 3X + 1")
print("------------------")
print("        4        ")
x = int(input("\nX = "))
res = ((x**2) + (3*x) + 1)/4
print(f"\nSu resultado es {res}")

# Ejercicio 4
nom2 = input("\nIngrese su nombre: ")
rut = input("\nIngrese su rut (agregue puntos y guión): ")
correo = input("\nAgrega tu correo (Con @ y .com): ")
telef = input("\nIngresa tu teléfono: ")

print(f"\nNOMBRE:           {nom2.upper()}")
print(f"RUT:              {rut.upper()}")
print(f"CORREO:           {correo.upper()}")
print(f"TELEFONO:         {telef.upper()}")
